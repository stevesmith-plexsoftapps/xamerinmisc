﻿using System;
using System.ComponentModel;

namespace DataBinding
{
    public class MainViewModel : INotifyPropertyChanged 
    {
        private bool isDirty = false;
        public bool IdDirty { get { return isDirty; } }

        string name = "";
        public string Name
        {
            get => name;

            set
            {
                if (name == value)
                {
                    return;
                }

                name = value;
                OnPropertyChanged(nameof(Name));
                OnPropertyChanged(nameof(DisplayName));
            }
        }

        public string DisplayName
        {
            get
            {
                if (!string.IsNullOrEmpty(Name))
                {
                    return $"Name Entered: {Name}";
                }
                else
                {
                    return "No name entered yet";
                }
            }
        }

        public MainViewModel()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string name)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
